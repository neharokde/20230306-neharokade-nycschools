# 20230306-NehaRokade-NYCSchools

# NYC Schools

<p>
   The app displays list of HighSchools in NYC and its details. 
   On launch of app, shows a list of DOE High School Directory of 2017. 
   when selecting a school showing additional information on school details page.
</p>

## Technical Details

* This app is structured with a MVVM architecture.

* Retrofit used as the web client.

* Kotlin Coroutine for executing task asynchronously.

* Jetpack Navigation component for screen navigation.

* There are also some unit tests to check that I am properly receiving and parsing the data from the api.
