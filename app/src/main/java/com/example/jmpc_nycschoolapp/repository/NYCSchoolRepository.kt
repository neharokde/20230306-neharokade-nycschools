package com.example.jmpc_nycschoolapp.repository

import com.example.jmpc_nycschoolapp.api.NYCSchoolApi
import com.example.jmpc_nycschoolapp.model.School
import com.example.jmpc_nycschoolapp.model.SchoolDetails
import com.example.jmpc_nycschoolapp.utils.NetworkResult
import retrofit2.Response

class NYCSchoolRepository(private val nycSchoolApi: NYCSchoolApi) {

    //    Response<ArrayList<School>>
    suspend fun getSchools(): NetworkResult<List<School>> {
        val response = nycSchoolApi.getSchoolsList()
        return if (response.isSuccessful) {
            val responseBody = response.body()
            if (responseBody != null) {
                NetworkResult.Success(responseBody)
            } else {
                NetworkResult.Error("Something went wrong")
            }
        } else {
            NetworkResult.Error("Something went wrong")
        }
    }

    //    Response<Array<SchoolDetails>>
    suspend fun getSchoolDetails(dbn: String): NetworkResult<Array<SchoolDetails>> {
        val response = nycSchoolApi.getSchoolDetails(dbn)
        return if (response.isSuccessful) {
            val responseBody = response.body()
            if (responseBody != null) {
                NetworkResult.Success(responseBody)
            } else {
                NetworkResult.Error("Something went wrong")
            }
        } else {
            NetworkResult.Error("Something went wrong")
        }
    }

}