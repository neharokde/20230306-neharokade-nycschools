package com.example.jmpc_nycschoolapp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.jmpc_nycschoolapp.adapter.OnItemClickListener
import com.example.jmpc_nycschoolapp.adapter.SchoolListAdapter
import com.example.jmpc_nycschoolapp.databinding.FragmentSchoolListBinding
import com.example.jmpc_nycschoolapp.model.School
import com.example.jmpc_nycschoolapp.repository.NYCSchoolRepository
import com.example.jmpc_nycschoolapp.utils.NetworkResult
import com.example.jmpc_nycschoolapp.viewmodels.schoollist.SchoolListViewModel
import com.example.jmpc_nycschoolapp.viewmodels.schoollist.SchoolListViewModelFactory


class SchoolListFragment : Fragment(), OnItemClickListener {
    private lateinit var repository: NYCSchoolRepository
    private var _binding: FragmentSchoolListBinding? = null
    private val binding get() = _binding!!

    private lateinit var schoolListViewModel: SchoolListViewModel
    private lateinit var schoolListAdapter: SchoolListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        repository = (activity?.application as NYCSchoolApp).nycSchoolRepository
        schoolListViewModel = ViewModelProvider(
            this,
            SchoolListViewModelFactory(repository)
        )[SchoolListViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSchoolListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.progressBar.visibility = View.VISIBLE
        binding.errorText.visibility = View.GONE
        binding.schoolsList.layoutManager = LinearLayoutManager(binding.root.context)

        schoolListViewModel.getSchoolList()
        schoolListViewModel.schools.observe(viewLifecycleOwner, Observer {
            when (it) {
                is NetworkResult.Success -> {
                    Log.d("TAG", it.data.toString())
                    binding.progressBar.visibility = View.GONE
                    binding.errorText.visibility = View.GONE
                    schoolListAdapter = SchoolListAdapter(it.data!!, this)
                    binding.schoolsList.adapter = schoolListAdapter
                }
                is NetworkResult.Error -> {
                    binding.progressBar.visibility = View.GONE
                    binding.errorText.visibility = View.VISIBLE
                    binding.errorText.text = it.message
                }

                else -> {
                    binding.progressBar.visibility = View.GONE
                    binding.errorText.visibility = View.VISIBLE
                }
            }
        })

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onItemClick(school: School) {
        Log.i("School dbn ", " : ${school.dbn}" )
        val bundle = bundleOf("dbn" to school.dbn)
        view?.let { Navigation.findNavController(it) }
            ?.navigate(R.id.action_schoolListFragment_to_schoolDetailsFragment, bundle)
    }
}

