package com.example.jmpc_nycschoolapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.jmpc_nycschoolapp.R
import com.example.jmpc_nycschoolapp.model.School

class SchoolListAdapter(
    private val schoolList: List<School>,
    private val itemClickListener: OnItemClickListener
) : RecyclerView.Adapter<SchoolListAdapter.SchoolsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolsViewHolder {

        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.schools_row_item, parent, false)
        return SchoolsViewHolder(view)
    }

    override fun onBindViewHolder(holder: SchoolsViewHolder, position: Int) {
        val school = schoolList[position]
        holder.schoolName.text = school.schoolName
        holder.schoolAddress.text = school.primaryAddressLine1
        holder.schoolCity.text = school.city
        holder.schoolState.text = school.stateCode
        holder.schoolZip.text = school.zip
        holder.bind(school, itemClickListener)
    }

    override fun getItemCount(): Int {
        return schoolList.size
    }

    class SchoolsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var schoolName = itemView.findViewById<AppCompatTextView>(R.id.tv_schoolName)
        var schoolAddress = itemView.findViewById<AppCompatTextView>(R.id.tv_schoolAddress)
        var schoolCity = itemView.findViewById<AppCompatTextView>(R.id.tv_schoolCity)
        var schoolState = itemView.findViewById<AppCompatTextView>(R.id.tv_schoolState)
        var schoolZip = itemView.findViewById<AppCompatTextView>(R.id.tv_schoolZip)

        fun bind(school: School, clickListener: OnItemClickListener) {
            itemView.setOnClickListener { clickListener.onItemClick(school) }
        }

    }

}


interface OnItemClickListener {
    fun onItemClick(school: School)
}