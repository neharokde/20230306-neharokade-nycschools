package com.example.jmpc_nycschoolapp.model


import com.google.gson.annotations.SerializedName

data class School(
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("primary_address_line_1")
    val primaryAddressLine1: String,
    @SerializedName("city")
    val city: String,
    @SerializedName("state_code")
    val stateCode: String,
    @SerializedName("zip")
    val zip: String
)