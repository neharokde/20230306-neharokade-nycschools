package com.example.jmpc_nycschoolapp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.jmpc_nycschoolapp.databinding.FragmentSchoolDetailsBinding
import com.example.jmpc_nycschoolapp.model.SchoolDetails
import com.example.jmpc_nycschoolapp.repository.NYCSchoolRepository
import com.example.jmpc_nycschoolapp.utils.NetworkResult
import com.example.jmpc_nycschoolapp.viewmodels.schooldetails.SchoolDetailsViewModel
import com.example.jmpc_nycschoolapp.viewmodels.schooldetails.SchoolDetailsViewModelFactory
import com.example.jmpc_nycschoolapp.viewmodels.schoollist.SchoolListViewModelFactory

class SchoolDetailsFragment : Fragment() {
    private var _binding: FragmentSchoolDetailsBinding? = null
    private val binding get() = _binding!!

    private lateinit var schoolDetailsViewModel: SchoolDetailsViewModel
    lateinit var dbn: String
    private lateinit var repository: NYCSchoolRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        repository = (activity?.application as NYCSchoolApp).nycSchoolRepository
        schoolDetailsViewModel = ViewModelProvider(
            this,
            SchoolDetailsViewModelFactory(repository)
        )[SchoolDetailsViewModel::class.java]

        dbn = arguments?.getString("dbn").toString()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSchoolDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.progressBar.visibility = View.VISIBLE

        schoolDetailsViewModel.getSchoolDetails(dbn)


        schoolDetailsViewModel.schoolsDetails.observe(viewLifecycleOwner) {
            when (it) {
                is NetworkResult.Success -> {
                    Log.d("TAG", it.data.contentToString())
                    binding.progressBar.visibility = View.GONE
                    val schoolDetailsArray: Array<SchoolDetails>? = it.data
                    if (schoolDetailsArray != null && schoolDetailsArray.isNotEmpty()) {
                        binding.schoolDetailLayout.visibility= View.VISIBLE
                        binding.tvErrorMessage.visibility = View.GONE
                        for (i in schoolDetailsArray.indices) {
                            if (dbn == schoolDetailsArray[i].dbn) {
                                binding.tvSchoolName.text = schoolDetailsArray[i].schoolName
                                binding.tvNoOfStudent.text =
                                    schoolDetailsArray[i].numOfSatTestTakers
                                binding.tvMathAvgValue.text = schoolDetailsArray[i].satMathAvgScore
                                binding.tvReadingAvgValue.text =
                                    schoolDetailsArray[i].satCriticalReadingAvgScore
                                binding.tvWritingAvgValue.text =
                                    schoolDetailsArray[i].satWritingAvgScore
                            }
                        }
                    }else{
                        binding.schoolDetailLayout.visibility= View.GONE
                        binding.tvErrorMessage.visibility = View.VISIBLE
                        binding.tvErrorMessage.text = "No Record Found"
                    }

                }
                is NetworkResult.Error -> {
                    binding.progressBar.visibility = View.GONE

                }

                else -> {
                    binding.progressBar.visibility = View.GONE

                }
            }
        }


    }

    private fun showDataonView(it: NetworkResult<Array<SchoolDetails>>?) {

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}