package com.example.jmpc_nycschoolapp.api

import com.example.jmpc_nycschoolapp.model.School
import com.example.jmpc_nycschoolapp.model.SchoolDetails
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NYCSchoolApi {

    //https://data.cityofnewyork.us/resource/s3k6-pzi2.json
    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchoolsList(): Response<ArrayList<School>>

    //https://data.cityofnewyork.us/resource/f9bf-2cp4.json
    @GET("resource/f9bf-2cp4.json")
    suspend fun getSchoolDetails(@Query("dbn") dbn: String): Response<Array<SchoolDetails>>




}