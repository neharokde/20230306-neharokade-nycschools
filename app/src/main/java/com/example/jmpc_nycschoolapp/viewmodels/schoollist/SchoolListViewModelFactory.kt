package com.example.jmpc_nycschoolapp.viewmodels.schoollist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.jmpc_nycschoolapp.repository.NYCSchoolRepository

class SchoolListViewModelFactory(private val nycSchoolRepository: NYCSchoolRepository) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return SchoolListViewModel(nycSchoolRepository) as T
    }
}