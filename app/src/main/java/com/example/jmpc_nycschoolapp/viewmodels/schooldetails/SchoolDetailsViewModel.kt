package com.example.jmpc_nycschoolapp.viewmodels.schooldetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.jmpc_nycschoolapp.model.SchoolDetails
import com.example.jmpc_nycschoolapp.repository.NYCSchoolRepository
import com.example.jmpc_nycschoolapp.utils.NetworkResult
import kotlinx.coroutines.launch

class SchoolDetailsViewModel(private val nycSchoolRepository: NYCSchoolRepository) : ViewModel() {

    private val _schoolsDetails = MutableLiveData<NetworkResult<Array<SchoolDetails>>>()
    val schoolsDetails: LiveData<NetworkResult<Array<SchoolDetails>>>
        get() = _schoolsDetails


    fun getSchoolDetails(dbn: String) {
        viewModelScope.launch {
            val result = nycSchoolRepository.getSchoolDetails(dbn)
            _schoolsDetails.postValue(result)

        }
    }


}