package com.example.jmpc_nycschoolapp.viewmodels.schooldetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.jmpc_nycschoolapp.repository.NYCSchoolRepository
import com.example.jmpc_nycschoolapp.viewmodels.schoollist.SchoolListViewModel

class SchoolDetailsViewModelFactory(private val nycSchoolRepository: NYCSchoolRepository) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return SchoolDetailsViewModel(nycSchoolRepository) as T
    }
}