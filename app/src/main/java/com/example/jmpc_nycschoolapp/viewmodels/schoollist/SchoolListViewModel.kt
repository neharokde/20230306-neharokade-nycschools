package com.example.jmpc_nycschoolapp.viewmodels.schoollist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.jmpc_nycschoolapp.model.School
import com.example.jmpc_nycschoolapp.repository.NYCSchoolRepository
import com.example.jmpc_nycschoolapp.utils.NetworkResult
import kotlinx.coroutines.launch

class SchoolListViewModel(private val nycSchoolRepository: NYCSchoolRepository) : ViewModel() {

    private val _schools = MutableLiveData<NetworkResult<List<School>>>()
    val schools: LiveData<NetworkResult<List<School>>>
    get() = _schools

    fun getSchoolList(){
        viewModelScope.launch {
            val result = nycSchoolRepository.getSchools()
            _schools.postValue(result)
        }
    }

}