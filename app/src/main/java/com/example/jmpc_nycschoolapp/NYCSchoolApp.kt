package com.example.jmpc_nycschoolapp

import android.app.Application
import com.example.jmpc_nycschoolapp.api.NYCSchoolApi
import com.example.jmpc_nycschoolapp.repository.NYCSchoolRepository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class NYCSchoolApp : Application() {

    private val BASE_URL = "https://data.cityofnewyork.us/"

    lateinit var nycSchoolApi: NYCSchoolApi
    lateinit var nycSchoolRepository: NYCSchoolRepository

    override fun onCreate() {
        super.onCreate()

        val interceptor = HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        }

        val client = OkHttpClient.Builder().apply {
            this.addInterceptor(interceptor).connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(25, TimeUnit.SECONDS)
        }.build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()


        nycSchoolApi = retrofit.create(NYCSchoolApi::class.java)
        nycSchoolRepository = NYCSchoolRepository(nycSchoolApi)

    }
}