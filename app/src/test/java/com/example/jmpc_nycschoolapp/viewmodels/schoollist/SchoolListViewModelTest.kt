package com.example.jmpc_nycschoolapp.viewmodels.schoollist


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.jmpc_nycschoolapp.getOrAwaitValue
import com.example.jmpc_nycschoolapp.repository.NYCSchoolRepository
import com.example.jmpc_nycschoolapp.utils.NetworkResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class SchoolListViewModelTest {

    private val testDispatcher = StandardTestDispatcher()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var repository: NYCSchoolRepository


    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
    }

    @Test
    fun test_GetSchoolsList()= runTest{
        Mockito.`when`(repository.getSchools()).thenReturn(NetworkResult.Success(emptyList()))
        val systemUnderTest = SchoolListViewModel(repository)
        systemUnderTest.getSchoolList()
        testDispatcher.scheduler.advanceUntilIdle()
        val result = systemUnderTest.schools.getOrAwaitValue()
        Assert.assertEquals(0, result.data!!.size)
    }

    @Test
    fun test_GetSchoolsList_expectedError()= runTest{
        Mockito.`when`(repository.getSchools()).thenReturn(NetworkResult.Error("Something went wrong"))
        val systemUnderTest = SchoolListViewModel(repository)
        systemUnderTest.getSchoolList()
        testDispatcher.scheduler.advanceUntilIdle()
        val result = systemUnderTest.schools.getOrAwaitValue()
        Assert.assertEquals(true, result is NetworkResult.Error)
        Assert.assertEquals("Something went wrong", result.message)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}