package com.example.jmpc_nycschoolapp.repository

import android.os.Build.VERSION_CODES.M
import com.example.jmpc_nycschoolapp.api.NYCSchoolApi
import com.example.jmpc_nycschoolapp.model.School
import com.example.jmpc_nycschoolapp.utils.NetworkResult
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

class NYCSchoolRepositoryTest {

    @Mock
    lateinit var nycSchoolApi: NYCSchoolApi

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }
    @Test
    fun testGetSchools_EmptyList()=runTest {
        val emptyList = ArrayList<School>()
        Mockito.`when`(nycSchoolApi.getSchoolsList()).thenReturn(Response.success(emptyList))
        val sut = NYCSchoolRepository(nycSchoolApi)
        val result = sut.getSchools()
        Assert.assertEquals(true, result is NetworkResult.Success)
        Assert.assertEquals(0, result.data!!.size)

    }

    @Test
    fun testGetSchools_expectedSchoolList() = runTest {
        val schooolList = arrayListOf<School>(
            School(dbn="21K728", schoolName="Liberation Diploma Plus High School", primaryAddressLine1="2865 West 19th Street", city="Brooklyn", stateCode="NY", zip="11224"),
            School(dbn="08X282", schoolName="Women's Academy of Excellence", primaryAddressLine1="456 White Plains Road", city="Bronx", stateCode="NY", zip="10473")
        )
        Mockito.`when`(nycSchoolApi.getSchoolsList()).thenReturn(Response.success(schooolList))

        val sut = NYCSchoolRepository(nycSchoolApi)
        val result = sut.getSchools()
        Assert.assertEquals(true, result is NetworkResult.Success)
        Assert.assertEquals(2, result.data!!.size)
        Assert.assertEquals("21K728", result.data!![0].dbn)
    }


}